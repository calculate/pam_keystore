INSTALL	:= install
DESTDIR	:=
PIC	:= -fPIC
LD_D	:= $(CC) -shared
CFLAGS	:=
SHARE	:= -g $(PIC)


pam_keystore: pam_keystore.c
	$(CC) $(CFLAGS) $(SHARE) -c pam_keystore.c -o pam_keystore.o
	$(LD_D)  -o pam_keystore.so pam_keystore.o  -lc  -lkeyutils -lpam

clean: pam_keystore.so pam_keystore.o
	$(RM) pam_keystore.o
	$(RM) pam_keystore.so
	
install: pam_keystore.so
	$(INSTALL) -D -m 0755 pam_keystore.so $(DESTDIR)/lib/security/pam_keystore.so

uninstall: $(DESTDIR)/lib/security/pam_keystore.so
	$(RM) $(DESTDIR)/lib/security/pam_keystore.so